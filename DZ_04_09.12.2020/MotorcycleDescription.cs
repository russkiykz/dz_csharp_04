﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_04_09._12._2020
{
    public partial class Motorcycle
    {
        public void PrintInfo()
        {
            Console.WriteLine($"Марка: {_brand}.");
            Console.WriteLine("Характеристики: ");
            Console.WriteLine($"Объем двигателя: {_engineVolume} куб. м");
            Console.WriteLine($"Максимальная скорость: {_maxSpeed} км/ч");
            Console.WriteLine($"объем топливного бака: {_fuelTankVolume} л.");
            Console.WriteLine($"Вес: {_weight} кг.");
            Console.WriteLine($"Количество колес: {_wheelsQuantity}");
            Console.WriteLine($"Тип тормозов: {_brakeType}");
        }
    }
}
