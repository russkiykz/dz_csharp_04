﻿using System;

namespace DZ_04_09._12._2020
{
    class Program
    {
        static void Main(string[] args)
        {
            Motorcycle[] motorcycles = new Motorcycle[5] 
            { 
                new Motorcycle("Yamaha",2,300,20,170),            
                new Motorcycle("Suzuki",1.5,250,15,150),            
                new Motorcycle("Honda",1,180,10,120),            
                new Motorcycle("BMW",2.5,360,25,190),            
                new Motorcycle("Kawasaki",3,400,30,200)            
            };
            foreach(Motorcycle motorcycle in motorcycles )
            {
                motorcycle.PrintInfo();
                Console.WriteLine("------");
            }
        }
    }
}
