﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DZ_04_09._12._2020
{
    public partial class Motorcycle
    {
        private string _brand; // марка мотоцикла
        private double _engineVolume; // объем двигателя
        private int _maxSpeed; // максимальная скорость
        private int _fuelTankVolume; // объем топливного бака
        private double _weight; // вес
        private static int _wheelsQuantity=2; // Количество колес
        private static string _brakeType = "Дисковые"; // тип тормозов
        // Методы для управления классом
        public string GetBrand()
        {
            return _brand;
        }
        public void SetBrand(string brand)
        {
            _brand = brand;
        }
        public double GetEngineVolume()
        {
            return _engineVolume;
        }
        public void SetEngineVolume(double engineVolume)
        {
            _engineVolume = engineVolume;
        }
        public int GetMaxSpeed()
        {
            return _maxSpeed;
        }
        public void SetMaxSpeed(int maxSpeed)
        {
            _maxSpeed = maxSpeed;
        }
        public int GetFuelTankVolume()
        {
            return _fuelTankVolume;
        }
        public void SetFuelTankVolume(int fuelTankVolume)
        {
            _fuelTankVolume = fuelTankVolume;
        }
        public double GetWeight()
        {
            return _weight;
        }
        public void SetWeight(double weight)
        {
            _weight = weight;
        }
        // Конструкторы
        public Motorcycle() { }
        public Motorcycle(string brand, double engineVolume)
        {
            _brand = brand;
            _engineVolume = engineVolume;
        }
        public Motorcycle(string brand, double engineVolume, int maxSpeed, int fuelTankVolume, double weight)
        {
            _brand = brand;
            _engineVolume = engineVolume;
            _maxSpeed = maxSpeed;
            _fuelTankVolume = fuelTankVolume;
            _weight = weight;
        }
        // Статический конструктор
        static Motorcycle()
        {
            Console.WriteLine("Homework #4\nClass Motocycle\n");
        }      
    }
}
